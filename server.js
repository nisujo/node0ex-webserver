const express = require('express');
const app = express();
const hbs = require('hbs');

require('./hbs/helpers');


// Variable usada por Heroku
const port = process.env.PORT || 3000;



app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');



// Servir contenido estatico
app.use(express.static(__dirname + '/public'));



app.get('/', (req, res) => {

    res.render('home', {
        name: 'Jose'
    });

});

app.get('/about', (req, res) => {

    res.render('about');

});

app.listen(port, () => {

    console.log(`Listening on port:${port}`);

});